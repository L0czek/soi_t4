#pragma once

#include "monitor.h"

#include <deque>

class Buffer :public Monitor {
    Condition oddNumberPush;
    Condition oddNumberPop;
    Condition evenNumberPush;
    Condition evenNumberPop;

    std::deque<std::size_t> queue;
public:
    Buffer() = default;
    void pushOddNumber(std::size_t n);
    void pushEvenNumber(std::size_t n);
    std::size_t popOddNumber();
    std::size_t popEvenNumber();
    
    template<typename Func>
        void for_each(Func&& func) {
            enter();

            for(const auto &i : queue) {
                func(i);
            }

            leave();
        }

    template<typename Func>
        void call_sync(Func&& func) {
            enter();

            func();

            leave();
        }

    bool canPushOddNumber();
    bool canPushEvenNumber();
    bool canPopOddNumber();
    bool canPopEvenNumber();
};
