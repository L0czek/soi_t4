#include "buffer.hpp"

void Buffer::pushOddNumber(std::size_t n) {
    enter();
    
    if(!canPushOddNumber()) {
        wait(oddNumberPush);
    }

    queue.push_front(n);

    if(canPopEvenNumber()) {
        signal(evenNumberPop);
    } else if(canPopOddNumber()) {
        signal(oddNumberPop);
    } else if(canPushEvenNumber()) {
        signal(evenNumberPush);
    }    leave();
}
void Buffer::pushEvenNumber(std::size_t n) {
    enter();
    
    if(!canPushEvenNumber()) {
        wait(evenNumberPush);
    }

    queue.push_front(n);
    
    if(canPushOddNumber()) {
        signal(oddNumberPush);
    } else if(canPopEvenNumber()) {
        signal(evenNumberPop);
    } else if(canPopOddNumber()) {
        signal(oddNumberPop);
    }
    leave();
}
std::size_t Buffer::popOddNumber() {
     enter();
    
    if(!canPopOddNumber()) {
        wait(oddNumberPop);
    }

    auto ret = queue.back();
    queue.pop_back();
    
    if(canPushEvenNumber()) {
        signal(evenNumberPush);
    } else if(canPushOddNumber()) {
        signal(oddNumberPush);
    } else if(canPopEvenNumber()) {
        signal(evenNumberPop);
    } 
    leave();
    return ret;
}
std::size_t Buffer::popEvenNumber() {
  enter();
    
    if(!canPopEvenNumber()) {
        wait(evenNumberPop);
    }

    auto ret = queue.back();
    queue.pop_back();
    
    if(canPopOddNumber()) {
        signal(oddNumberPop);
    } else if(canPushOddNumber()) {
        signal(oddNumberPush);
    } else if(canPushEvenNumber()) {
        signal(evenNumberPush);
    } 
leave();
    return ret;
}
   
bool Buffer::canPushOddNumber() {
    std::size_t even_num = 0;
    std::size_t odd_num = 0;
    for(const auto &i : queue) {
        if(i % 2 == 0) {
            even_num++;        
        } else {
            odd_num++;
        }
    }
    return odd_num < even_num;
}
bool Buffer::canPushEvenNumber() {
    std::size_t even_num = 0;
    for(const auto &i : queue) {
        if(i % 2 == 0) {
            even_num++;
        }
    }
    return even_num < 10;
}
bool Buffer::canPopOddNumber() {
    return queue.size() >= 7 && queue.back() % 2 == 1;
}
bool Buffer::canPopEvenNumber() {
    return queue.size() >= 3 && queue.back() % 2 == 0;
}

