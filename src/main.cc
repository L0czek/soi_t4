#include <iostream>
#include <thread>
#include "buffer.hpp"
#include <iterator>
#include <vector>

using namespace std::chrono_literals;

struct OddProducer {
    static std::thread spawn(Buffer& buffer) {
        return std::thread([&buffer](){
                for(std::size_t i=0; true; i=(i+1)%100) {
                    std::size_t n = 2*i + 1;
                    buffer.pushOddNumber(n);
                    std::this_thread::sleep_for(5ms);
                }
                });
    }
};

struct EvenProducer {
    static std::thread spawn(Buffer& buffer) {
        return std::thread([&buffer](){
                for(std::size_t i=0; true; i=(i+1)%100) {
                    std::size_t n = 2*i;
                    buffer.pushEvenNumber(n);
                std::this_thread::sleep_for(5ms);
                    }
                });
    }
};

struct OddConsumer {
    static std::thread spawn(Buffer& buffer) {
        return std::thread([&buffer](){
                for(;;) {
                    buffer.popOddNumber();
                std::this_thread::sleep_for(5ms);
                    } 
                });
    }
};

struct EvenConsumer {
    static std::thread spawn(Buffer& buffer) {
        return std::thread([&buffer](){
                for(;;) {
                    buffer.popEvenNumber();
                std::this_thread::sleep_for(5ms);
                    }
                }); 
    }
};

template<typename T>
void spawn(std::size_t n, Buffer & buffer, std::back_insert_iterator<std::vector<std::thread>> it) {
    for(std::size_t i=0; i < n; ++i) {
        it = T::spawn(buffer);
    }
}

int main() {
   Buffer buf;
   std::vector<std::thread> th;
   buf.call_sync([&](){
                spawn<OddProducer>(2, buf,  std::back_inserter(th));
                spawn<EvenProducer>(2, buf, std::back_inserter(th));
                spawn<OddConsumer>(2, buf, std::back_inserter(th));
                spawn<EvenConsumer>(2, buf, std::back_inserter(th));
           });

   for(;;) {
       buf.for_each([](auto i){
                    std::cout << i << ", ";
               });
       puts("\n");
       std::this_thread::sleep_for(1s);
   }
}
